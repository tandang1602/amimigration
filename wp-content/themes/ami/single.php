<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$related = new AmiPost();
$context['related'] = $related->get_related_auto($post->ID);
Timber::render( 'single.twig', $context );
?>
