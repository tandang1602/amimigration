<?php
$context = Timber::get_context();
$category = get_queried_object();
$banner = get_field('banner_image');
$context['banner'] = $banner['url'];
$context['category'] = $category;
Timber::render( 'blog-overview.twig', $context );
?>

