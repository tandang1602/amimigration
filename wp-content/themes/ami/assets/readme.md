This Gulp template should be run on [Sublime Text 3]
    1.Configre [Sublime Text 3]
        + Choose [Preferences --> Settings-user] and paste code bellow
            {
                "atomic_save": true,
                "font_size": 14,
                "ignored_packages":
                [
                    "Vintage"
                ],
                "translate_tabs_to_spaces": true,
                "trim_trailing_white_space_on_save": true,
                "rulers": [120],
                "word_wrap": true,
                "wrap_width": 120
            }
        + Save and restart ST3
        + Install package syntax for ST3: [Jade,SCSS]
    2.Install NodeJS at: [https://nodejs.org/en/] --> 4.4.7
        + Remember accept "Add to PATH"
    3.Install ruby-installer to use SCSS lint
        _ http://rubyinstaller.org/downloads/ (version: 2.2.3)
        _ gem install scss_lint
    4.Run gulp template
        + Open CMD at root folder and type "npm install" (or Extract file node_modules.zip if you dont have Internet)
        + When you done above step, type
            "gulp" to build SCSS JS.
            "gulp deploy" to create CSS,JS min and criticle CSS .

    Note: To remove long path on Windows, Create "emtyfolder" and use this syntax on CMD : robocopy emtyfolder node_modules /purge
