/* Global variables and functions */
var ProjectName = (function($, window, undefined) {
    'use strict';
    var $win = $(window),
        $footer = $('#footer'),
        $header = $('#header'),
        $html = $('html'),
        $linkPanel = $('.link-panel'),
        $main = $('#main');

    function _footerBottom() { //required
        var h = $win.outerHeight() - $footer.outerHeight();
        $main.css('min-height', h);
    }

    function _wrapTable() {
        var $element = $(".wp-editor table");
        $element.wrap("<div class='table-scroll'/>");
    }

    function _initSelect() {
        var $select = $('.gform_wrapper select'),
            timeout;

        if ($select.length) {
            $win.on('resize.select2', function() {
                clearTimeout(timeout);

                timeout = setTimeout(function() {
                    $select.select2({
                        minimumResultsForSearch: Infinity,
                        dropdownCssClass: 'contact-dropdown'
                    });
                }, 200);
            }).trigger('resize.select2');
        }
    }

    function _menuMobile() {
        var currentMenuIcon = $('#main-menu li.current-menu-item > span.icon, ' +
            '#main-menu li.current-menu-ancestor > span.icon, ' +
            '#main-menu li.current-menu-parent > span.icon, ' +
            '#main-menu li.current-page-ancestor > span.icon,' +
            '#main-menu li.current-post-ancestor > span.icon');
        currentMenuIcon.addClass('active');

        $('#main-menu li span.icon').on('click', function() {
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });

        $('[data-off-canvas]').on('opened.zf.offcanvas', function() {
            $('body').addClass('open-menu');
        });

        $('[data-off-canvas]').on('closed.zf.offcanvas', function() {
            $('body').removeClass('open-menu');
        });
    }

    function _slider() {
        $('#banner-home').slick({
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            autoplay: true,
            fade: true,
        });

        $('.slick-next').click(function(){
            $('#banner-home').slick('slickNext');
        });

        $('.slick-prev').click(function(){
            $('#banner-home').slick('slickPrev');
        });

        $('.quote-block.slider > .row').slick({
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            dots: true,
        });

        $('.block-item .slider').slick({
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            dots: true,
            arrows: false,
        });
    }

    function _scrollAnchor() {
        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();
        
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
        });
    }

    function _map() {
        if($('#googleMap').length) {
            var $info = $('#googleMap');
            var lat = $info.attr('data-lat'),
                lng = $info.attr('data-lng'),
                title = $info.attr('data-title'),
                address = $info.attr('data-address'),
                icon = $info.attr('data-icon');
            var style = [
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#f1f2f7"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#93eabe"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#45d78e"
            }
        ]
    },
    {
        "featureType": "poi.sports_complex",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#93eabe"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fdaa30"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fdaa30"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fbdeb4"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#64cefc"
            }
        ]
    }
]

            var map = new google.maps.Map(document.getElementById('googleMap'), {
                zoom: 15,
                center: {lat: parseFloat(lat), lng: parseFloat(lng)},
                scrollwheel: false,
                styles: style,
            });

            var image = icon;
            var beachMarker = new google.maps.Marker({
                  position: {lat: parseFloat(lat), lng: parseFloat(lng)},
                  map: map,
                  icon: image
            });

            var contentString = '<div id="content">'+
                '<p class="firstHeading">' + title +'</p>'+
                '<div id="bodyContent">'+
                '<p>'+ address+'</p>'
                '</div>'+
                '</div>';

            var myOptions = {
                content: contentString,
                pixelOffset: new google.maps.Size(-155, -65),
                position:  map.getCenter(),
                closeBoxURL: "",
                alignBottom: true,
                isHidden: false,
                enableEventPropagation: true
            };

            if(title !== '' || address !== '') {
                var ibLabel = new InfoBox(myOptions);
            }

            beachMarker.addListener('click', function() {
                ibLabel.open(map);
            });
        }
    }

    function _bindEvent() {
        var timeOutResize;

        $(document).bind('gform_post_render', function() {
            _initSelect();
        });

        $win.on('load', function() {
            _footerBottom();
        });

        $('.sticky-block__title').on('click', function() {
            $(this).toggleClass('active');
            $('.sticky-block__content').slideToggle();
        });

        $('a.open_popup').magnificPopup({
            type:'inline',
            midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });

        $('.banner__item_video').magnificPopup({
            type: 'iframe',
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com',
                        id: 'v=',
                        src: 'h%id%?autoplay=1'
                    }
                }
            },

            callbacks: {
                open: function() {
                    $('#banner-home').slick('slickSetOption', 'autoplay', false).slick('slickPause');
                },

                close: function() {
                    $('#banner-home').slick('slickPlay');
                }
            }
        });

        var currentWidth = $win.width();
        $win.on('resize', function() {
            clearTimeout(timeOutResize);
            timeOutResize = setTimeout(function() {
                var newWidth = $win.width();

                if (newWidth !== currentWidth) {
                    // Add function after resize windows
                    _footerBottom();

                    currentWidth = newWidth;
                }
            }, 200);
        });
    }

    return {
        init: function() {
            $(document).foundation();
            _bindEvent();
            _map();
            _wrapTable();
            _initSelect();
            _menuMobile();
            _slider();
            _scrollAnchor();
        }
    };
}(jQuery, window));

jQuery(document).ready(function() {
    ProjectName.init();
});