<?php
add_action('wp_ajax_dev_ajax_load_blog', 'dev_ajax_load_blog');
add_action('wp_ajax_nopriv_dev_ajax_load_blog', 'dev_ajax_load_blog');

function dev_ajax_load_blog(){
    ob_start();
    $return_posts = '';
    $number = get_option('posts_per_page');
    $page = $_POST['page'];
    $pageID = $_POST['pageID'];
    $termID = $_POST['termID'];
    $post_type = 'post';

    $args = array(
        'posts_per_page'   => $number,
        'post_type'        	=> $post_type,
        'post_status'      	=> 'publish',

    );

    if(!empty($termID)) {
        $args['cat'] = $termID;
    }

    if ( $page > 1 ) {
        $args['offset'] = ( $page - 1 ) * $number;
    }

    $posts 			= new WP_Query($args);
    $post_count		= $posts->found_posts;
    foreach ($posts->posts as $item) {
        include(locate_template("project/blog-item.php"));
    }

    $return_posts .= ob_get_clean();

    if(!empty($page)){
        $max_num_pages 		= round(ceil($post_count/$number));

        $return_pagination 	= dev_get_paginate_links($max_num_pages, $page ,$pageID);
    }

    if(empty($return_posts)){
        $return_status 		= "404";
    }
    if($return_status == "404"){
        $error_message 		= __("No results found");
        $return_posts  		= "<div class='column'><p class='error-msg'>" .$error_message. "</p></div>";
        $post_count	   		= 0;
        $return_pagination 	= "";
    }
    $result = array();
    $result['post_count'] 	= $post_count;
    $result['data'] 		= $return_posts;
    $result['pagination'] 	= $return_pagination;
    $result['post'] 		= $_POST;
    $result['args'] 		= $args;
    echo json_encode($result);
    exit();
}

