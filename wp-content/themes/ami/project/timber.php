<?php

class AmiPost extends TimberPost {

    function get_related_auto($postID) {
        if(!empty($postID)) {

            $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'post_status' => 'publish',
                    'post__not_in' => array($postID),
                    'orderby' => 'ID',
                    'order'   => 'DESC',
                );
            $related = Timber::get_posts($args);

            return $related;

        } else {
            return false;
        }
    }



}

?>