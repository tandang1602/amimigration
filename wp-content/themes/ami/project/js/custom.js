(function ($, root, undefined) {

    $(function () {
        var body = $("body");

        if (body.hasClass('page-template-template-blog') || body.hasClass('category')) {
            ajax_blog_filters(1);
            $(document).on('click', '.class_paging_blog a.page-numbers', function (event) {
                event.preventDefault();
                var link_href = $(this).attr('href');
                var link_array = link_href.split('/');
                var array_index = link_array.length - 2;
                var next_number = link_array[array_index];
                var next_page = next_number;
                ajax_blog_filters(next_page);
                return false;
            });
        }

        function ajax_blog_filters(page){
            var ajaxurl 			= theme_ajax.ajaxurl;
            var pagination_wrapper = $('.class_paging_blog');
            var load_posts_wrapper = $('.ajax-blog');
            var pageID =  load_posts_wrapper.data('page');
            var termID = load_posts_wrapper.data('category');
            $.ajax({
                type : 'post',
                url  : ajaxurl,
                data : {
                    action		 : 'dev_ajax_load_blog',
                    dataType	 : 'json',
                    page		 : page,
                    pageID       : pageID,
                    termID       : termID
                },
                beforeSend: function () {
                    if(page> 1) {
                        $('.icon-loading').show();
                    }
                },
                success: function (response) {
                    var obj = JSON.parse(response);
                    var html_posts 		= "";
                    var html_pagination = "";

                    if (obj.data !== null) {
                        html_posts = obj.data;
                    }

                    if (obj.pagination !== null) {
                        html_pagination = obj.pagination;
                    }

                    load_posts_wrapper.removeClass('loading');
                    $('.icon-loading').hide();
                    load_posts_wrapper.html(html_posts);
                    pagination_wrapper.html(html_pagination);
                    if(page > 0) {
                        jQuery('body, html').animate({
                            scrollTop: jQuery("#blog-data").offset().top - 110
                        }, 1500);
                    }
                },
                error: function (errorThrown) {
                    $('.icon-loading').hide();
                    console.log(errorThrown);
                }
            });
        }

    });
})(jQuery, this);
