<a class="columns small-12 medium-4" href="<?php echo get_permalink($item)?>">
    <figure><img src="<?php echo get_the_post_thumbnail_url($item, 'blog_item'); ?>" alt="<?php echo $item->post_title; ?>"></figure>
    <div class="caption">
        <p class="title"><?php echo $item->post_title; ?></p>
        <p class="intro"><?php echo dev_excerpt_max_charlength(130, $item); ?></p>
        <p class="text-right"><span class="btn-1" href="<?php echo get_permalink($item)?>"><?php echo __('Read more', 'ami')?></span></p>
    </div>
</a>