<?php

// require ajax function

require_once 'project/function_ajax.php';
require_once 'project/timber.php';

add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
add_theme_support('woocommerce');


add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
) );

$dev_nav_menus = array(
    'main' => __('Main Menu', $dev_textdomain),
);

register_nav_menus( $dev_nav_menus );

add_action('init', 'dev_scripts');
function dev_scripts()
{	
    if (!is_admin()) {
    	$src_js = THEME_URL.'/assets/js/main.min.js';
        wp_enqueue_script('fe-script', $src_js, array('jquery'), '1.0', true);

        $src_css = THEME_URL.'/assets/css/screen.min.css';
        wp_enqueue_style('fe-style', $src_css, array(), '1.0', 'all');

        $custom_css = THEME_URL.'/project/css/custom.css';
        wp_enqueue_style('cus-style', $custom_css, array(), '1.0', 'all');


        wp_enqueue_script('ajax-js',THEME_URL.'/project/js/custom.js', array('jquery'), '1.0.2',true );
        wp_localize_script( 'ajax-js', 'theme_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
     }
}

/**
 * Add ACF options page
 */
if ( function_exists( 'acf_add_options_page' ) ) {
    global $dev_textdomain;
    $parent = acf_add_options_page( __( 'Options', $dev_textdomain ) );

    // add sub page
    acf_add_options_page(array(
        'page_title'    => __( 'General', $dev_textdomain ),
        'menu_title'    => __( 'General', $dev_textdomain ),
        'parent_slug'   => $parent['menu_slug'],
    ));

    acf_add_options_sub_page(array(
        'page_title'    => __( 'Header', $dev_textdomain ),
        'menu_title'    => __( 'Header', $dev_textdomain ),
        'parent_slug'   => $parent['menu_slug'],
    ));

    acf_add_options_sub_page(array(
        'page_title'    => __( 'Footer', $dev_textdomain ),
        'menu_title'    => __( 'Footer', $dev_textdomain ),
        'parent_slug'   => $parent['menu_slug'],
    ));
}






