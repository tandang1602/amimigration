<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<!--[if lt IE 9]>
    <script src='<?php echo THEME_URL ?>/assets/js/html5shiv.min.js'></script>
    <script src='<?php echo THEME_URL ?>/assets/js/respond.min.js'></script>
    <![endif]-->
	<style type="text/css">
		html {
			margin-top: 0 !important;
		}
		body.admin-bar {
			padding-top: 32px !important;
		}
	</style>
</head>

<body <?php body_class(); ?>>

<?php
/**
 * Add Google Analytics tracking script
 *
 */
do_action('cus_add_google_tracking_code');
?>

<?php
/**
 * NOTE for dev:
 *
 * URL of theme folder: <?php echo THEME_URL ?>
 * URL of homepage: <?php echo home_url(); ?>
 *
 * Homepage: template-home.php
 * Default article: single.php
 * Default category: category.php
 *
 */
?>
<!-- wrapper -->
<div id="wrapper">
	<!-- header -->
	<header id="header">
		HTML: Header
	</header>
	<!-- /header -->

