<?php

/**
 * Define variable
 */
if (!defined('THEME_URL')) {
    define('THEME_URL', get_template_directory_uri());
}
if (!defined('CHILD_THEME_URL')) {
    define('CHILD_THEME_URL', get_stylesheet_directory_uri());
}

if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });

    add_filter('template_include', function ($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

add_action( 'after_setup_theme', 'dev_theme_setup' );
function dev_theme_setup() {
    add_image_size( 'blog_item', 525, 328, true );  
}

Timber::$dirname = array('templates');
Timber::$autoescape = false;

require_once TEMPLATEPATH . '/config.php';

add_filter( 'timber/context', 'add_to_context' );

function add_to_context( $context ) {
    $sticky = $footer = array();

    $context['options']['banner'] = get_field('opt_banner_default', 'options');

    $context['menu'] = new \Timber\Menu( 'main' );

    $context['header']['logo'] = get_field('opt_header_logo', 'option');

    $sticky['title'] = get_field('opt_sticky_title', 'option');
    $sticky['image'] = get_field('opt_sticky_image', 'option');
    $sticky['name'] = get_field('opt_sticky_name', 'option');
    $sticky['description'] = get_field('opt_sticky_description', 'option');
    $context['sticky'] = $sticky;

    //footer
    $footer['address_1'] = get_field('opt_footer_address_1', 'option');
    $footer['address_2'] = get_field('opt_footer_address_2', 'option');
    $footer['facebook'] = get_field('opt_footer_facebook', 'option');
    $footer['twitter'] = get_field('opt_footer_twitter', 'option');
    $footer['google'] = get_field('opt_footer_google', 'option');
    $footer['linkedin'] = get_field('opt_footer_linkedin', 'option');
    $footer['youtube'] = get_field('opt_footer_youtube', 'option');
    $footer['copyright'] = get_field('opt_footer_copyright', 'option');
    $footer['hotline'] = get_field('opt_footer_hotline', 'option');
    $footer['text_bottom'] = get_field('opt_footer_text_bottom', 'option');
    $footer['logo'] = get_field('opt_footer_logo', 'option');
    $footer['background'] = get_field('opt_footer_background', 'options');
   
    $context['footer'] = $footer;


    return $context;
}

function dev_excerpt_max_charlength($limit, $post) {
    if ( empty( $post ) ) {
        return '';
    }

    if ( ! empty( $post->post_excerpt ) ) {
        $content = $post->post_excerpt;
    } else {
        $content = $post->post_content;
    }

    return cut_tring_by_char( wp_strip_all_tags( $content ), $limit, '' );
}

function cut_tring_by_char( $string, $max_length, $more_string = ' ...' ) {
    if ( mb_strlen( $string, "UTF-8" ) > $max_length ) {
        $max_length = $max_length - 3;
        $string     = mb_substr( strip_tags( $string ), 0, $max_length, "UTF-8" );
        $pos        = strrpos( $string, " " );
        if ( $pos === false ) {
            return substr( $string, 0, $max_length ) . $more_string;
        }

        return substr( $string, 0, $pos ) . $more_string;
    } else {
        return $string;
    }
}

function dev_get_pagenum_link( $pagenum = 1, $base = null ) {
    $pagenum = (int) $pagenum;
    $request = $base ? remove_query_arg( 'paged', $base ) : remove_query_arg( 'paged' ). "/";
    return $request;
}
function dev_get_paginate_links($max_num_pages, $next_page = 1, $page_id = ""){
    $return = "";
    if(!empty($page_id)){
        $base_url = get_permalink($page_id);
        ob_start();
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( dev_get_pagenum_link($next_page, $base_url) ."page/%#%/" ) ),
            'total'        => $max_num_pages,
            'current'      => $next_page,
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 3,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => "",
            'next_text'    => "",
            'add_args'     => false,
            'add_fragment' => '',
        ) );
        $return .= ob_get_clean();
    }
    return $return;
}