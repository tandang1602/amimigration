<?php
/* Template Name: Blog Template */

$context = Timber::get_context();
$post = new TimberPost();
$banner = get_field('banner_image');
$context['banner'] = $banner['url'];
$context['post'] = $post;
Timber::render( 'blog-overview.twig', $context );
?>
