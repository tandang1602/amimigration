<?php
/* Template Name: Contact Template */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['theme_folder'] = THEME_URL.'/assets/images';
$sticky = array();
$sticky['title'] = get_field('opt_sticky_title', 'option');
$sticky['image'] = get_field('opt_sticky_image', 'option');
$sticky['name'] = get_field('opt_sticky_name', 'option');
$sticky['description'] = get_field('opt_sticky_description', 'option');
$context['sticky'] = $sticky;
Timber::render( 'page-contact.twig', $context );
?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADEorlK25R1V0WeC0zwcwyweiFAXII6S8"></script>
<script src="<?php echo THEME_URL.'/assets/js/infobox.js'; ?>"></script>